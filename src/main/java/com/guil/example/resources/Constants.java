package com.guil.example.resources;

import org.lwjgl.opengl.Display;

public class Constants {
    public static final String VERSION = "1.0-BETA";
    public static final String TITLE = "Beenetics" + " " + VERSION;
    public static final int WIDTH = 1440;
    public static final int HEIGHT = 820;

    public static final float APIARY_PRICE = 100.0F;
    public static final int SIDE_BAR_WIDTH = (int) (Display.getWidth() * 0.27);
}
