package com.guil.example.machines;

import com.guil.example.guis.BMachineGUI;

public abstract class BMachine {

    protected final float maxLife;
    protected BMachineGUI gui;
    protected boolean isStarted;
    protected float currentLife;

    public BMachine(float maxLife) {
        this.maxLife = maxLife;
        currentLife = this.maxLife;
    }

    public BMachineGUI getGui() {
        return gui;
    }

    public void startJourney() {
        isStarted = true;
    }

    public abstract void step();
}
