package com.guil.example.machines;

import com.guil.example.guis.BApiaryGUI;
import com.guil.interfaces.MouseListener;

import java.util.Random;

public class BApiary extends BMachine {

    public BApiary() {
        super(new Random().nextInt(100) + 100);
        gui = new BApiaryGUI();

        gui.getCloseButton().addMouseListener(new MouseListener() {
            @Override
            public void onHover() {

            }

            @Override
            public void onClick() {
                step();
            }
        });

        gui.setVisible(true);
    }

    @Override
    public void step() {
        if (currentLife > 0) {
            currentLife--;
        } else {
            currentLife = maxLife;
        }
        ((BApiaryGUI) gui).getLoadBar().setSize((int) (currentLife * 256 / maxLife), 6);
    }
}