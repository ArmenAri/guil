package com.guil.example.guis;

import com.guil.components.Label;
import com.guil.components.buttons.Button;
import com.guil.components.containers.Panel;
import com.guil.components.layouts.ListLayout;
import com.guil.enums.Alignment;
import com.guil.enums.Color;
import com.guil.enums.LabelType;
import lombok.Getter;
import org.lwjgl.opengl.Display;

@Getter
public class BMainMenuGUI extends Panel {

    private final Panel titlePanel;
    private final Label title;
    private final Button newGameButton, loadGameButton, quitButton;

    public BMainMenuGUI() {
        setLayout(new ListLayout());
        setSize(300, 300);
        setLocation(Display.getWidth() / 2 - 150, Display.getHeight() / 2 - 150);
        setBgColor(Color.TRANSPARENT);

        titlePanel = new Panel();
        titlePanel.setBgColor(Color.TRANSPARENT);
        titlePanel.setSize(280, 70);
        titlePanel.setPadding(0, 0);

        add(titlePanel);

        title = new Label("Beenetics", LabelType.TITLE);
        title.setColor(Color.DARK_GRAY);

        titlePanel.add(title);

        newGameButton = new Button("New Game");
        loadGameButton = new Button("Load Game");
        quitButton = new Button("Quit");

        newGameButton.setAlignment(Alignment.CENTER);
        loadGameButton.setAlignment(Alignment.CENTER);
        quitButton.setAlignment(Alignment.CENTER);

        add(newGameButton, loadGameButton, quitButton);
        newGameButton.occupyMaximumWidth();
        loadGameButton.occupyMaximumWidth();
        quitButton.occupyMaximumWidth();
    }

    public Button getNewGameButton() {
        return newGameButton;
    }

    public Button getQuitButton() {
        return quitButton;
    }
}
