package com.guil.example.guis.sidebar;

import com.guil.components.Label;
import com.guil.components.containers.Panel;
import com.guil.enums.Color;
import com.guil.enums.LabelType;
import com.guil.example.resources.Constants;
import org.lwjgl.opengl.Display;

public class BSideBarInfoGUI extends Panel {
    public BSideBarInfoGUI() {
        setSize(Constants.SIDE_BAR_WIDTH - 5, (int) (Display.getHeight() * 0.25));
        setLocation(0, 0);
        setBgColor(Color.MIDDLE_GRAY);

        add(new Label("Info", LabelType.TITLE));
    }
}
