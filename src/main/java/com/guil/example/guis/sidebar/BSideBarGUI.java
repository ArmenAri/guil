package com.guil.example.guis.sidebar;

import com.guil.components.containers.Panel;
import com.guil.enums.Color;
import com.guil.example.resources.Constants;
import lombok.Getter;
import org.lwjgl.opengl.Display;

@Getter
public class BSideBarGUI extends Panel {

    private final BSideBarInfoGUI sideBarInfoGUI;
    private final BSideBarStoreGUI sideBarStoreGUI;
    private final BSideBarBankGUI sideBarBankGUI;

    public BSideBarGUI() {
        setSize(Constants.SIDE_BAR_WIDTH, Display.getHeight());
        setBgColor(Color.DARK_GRAY);
        setPadding(0, 0);
        setLayout(null);

        sideBarInfoGUI = new BSideBarInfoGUI();
        sideBarStoreGUI = new BSideBarStoreGUI();
        sideBarBankGUI = new BSideBarBankGUI();

        add(sideBarInfoGUI, sideBarStoreGUI, sideBarBankGUI);
    }
}

