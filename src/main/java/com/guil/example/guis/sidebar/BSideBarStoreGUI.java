package com.guil.example.guis.sidebar;

import com.guil.components.Label;
import com.guil.components.buttons.Button;
import com.guil.components.containers.Panel;
import com.guil.components.layouts.ListLayout;
import com.guil.enums.Color;
import com.guil.enums.LabelType;
import com.guil.example.resources.Constants;
import lombok.Getter;
import org.lwjgl.opengl.Display;

@Getter
public class BSideBarStoreGUI extends Panel {
    private final Button buyApiaryButton;

    public BSideBarStoreGUI() {
        setSize(Constants.SIDE_BAR_WIDTH - 5, (int) (Display.getHeight() * 0.50));
        setLocation(0, (int) (Display.getHeight() * 0.25));
        setBgColor(Color.MIDDLE_GRAY);
        setPadding(5, 10);
        setLayout(new ListLayout());

        buyApiaryButton = new Button("Apiary " + Constants.APIARY_PRICE + "$");

        add(new Label("Store", LabelType.TITLE));
        add(buyApiaryButton);

        buyApiaryButton.occupyMaximumWidth();
    }
}
