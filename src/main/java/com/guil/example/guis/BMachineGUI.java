package com.guil.example.guis;

import com.guil.components.Label;
import com.guil.components.buttons.Button;
import com.guil.components.containers.Panel;
import com.guil.enums.Color;
import com.guil.enums.LabelType;
import lombok.Getter;

@Getter
public class BMachineGUI extends Panel {

    private final Panel topPanel;
    private final Label nameLabel;
    private final Button closeButton;

    public BMachineGUI(String name) {
        setBgColor(Color.LIGHT_GRAY);
        setSize(300, 150);
        setPadding(0, 0);

        topPanel = new Panel();
        topPanel.setBgColor(Color.GRAY);
        topPanel.setSize(300, 32);

        nameLabel = new Label(name, LabelType.TITLE);
        nameLabel.setLocation(4, 3);

        closeButton = new Button("x");
        closeButton.setFontSize(24);
        closeButton.setLocation(getSize().width - closeButton.getSize().width, 0);

        add(topPanel, nameLabel, closeButton);
    }
}
