package com.guil.example.guis;

import com.guil.components.buttons.Button;
import com.guil.components.containers.Panel;
import com.guil.enums.Color;
import lombok.Getter;

@Getter
public class BApiaryGUI extends BMachineGUI {

    private final Button recoverButton;
    private final Panel loadBar;

    public BApiaryGUI() {
        super("Apiary");
        recoverButton = new Button("|");
        recoverButton.setFontSize(24);
        recoverButton.setLocation(
                super.getSize().width - recoverButton.getSize().width,
                super.getSize().height - recoverButton.getSize().height
        );

        loadBar = new Panel();
        loadBar.setLocation(0, super.getSize().height - 6);
        loadBar.setSize(256, 6);
        loadBar.setBgColor(Color.RANDOM.newInstance());

        super.add(recoverButton, loadBar);
    }
}
