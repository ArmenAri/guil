package com.guil.example;

import com.guil.annotations.StartingActivity;
import com.guil.annotations.WindowInfo;
import com.guil.engine.GuilEngine;
import com.guil.example.resources.Constants;

@WindowInfo(icon = "/icons/icon.png", title = Constants.TITLE, width = Constants.WIDTH, height = Constants.HEIGHT)
@StartingActivity(name = "BMainActivity")
public class Beenetics {
    public static void main(String[] args) {
        GuilEngine.startGame(Beenetics.class);
    }
}
