package com.guil.example.activities;

import com.guil.activities.Activity;
import com.guil.activities.Intent;
import com.guil.annotations.GuilActivity;
import com.guil.engine.Window;
import com.guil.example.guis.BMainMenuGUI;
import com.guil.interfaces.MouseListener;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@GuilActivity
public class BMainActivity extends Activity {

    private final BMainMenuGUI mainMenuGUI;

    public BMainActivity() {
        super();
        mainMenuGUI = new BMainMenuGUI();

        mainMenuGUI.getNewGameButton().addMouseListener(new MouseListener() {
            @Override
            public void onHover() {

            }

            @Override
            public void onClick() {
                route(new Intent("BMainActivity", "BGameActivity"));
            }
        });

        mainMenuGUI.getQuitButton().addMouseListener(new MouseListener() {
            @Override
            public void onHover() {

            }

            @Override
            public void onClick() {
                Window.quit();
            }
        });

        start(mainMenuGUI);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onEnd() {

    }
}
