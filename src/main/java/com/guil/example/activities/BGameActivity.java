package com.guil.example.activities;

import com.guil.activities.Activity;
import com.guil.activities.Intent;
import com.guil.annotations.GuilActivity;
import com.guil.components.Notification;
import com.guil.components.buttons.Button;
import com.guil.components.containers.Panel;
import com.guil.components.layouts.GridLayout;
import com.guil.components.layouts.ListLayout;
import com.guil.enums.Color;
import com.guil.enums.NotificationType;
import com.guil.example.guis.sidebar.BSideBarGUI;
import com.guil.example.machines.BApiary;
import com.guil.example.resources.Constants;
import com.guil.interfaces.MouseListener;
import com.guil.managers.NotificationManager;
import lombok.Getter;
import org.lwjgl.opengl.Display;

import java.util.ArrayList;
import java.util.List;

@Getter
@GuilActivity(persistent = true)
public class BGameActivity extends Activity {

    private final Button closeButton;
    private final BSideBarGUI sidebarGUI;
    private final Panel mainGUI;

    private final List<BApiary> apiaries;

    public BGameActivity() {
        super();
        sidebarGUI = new BSideBarGUI();
        closeButton = new Button("x");
        apiaries = new ArrayList<>();
        closeButton.setLocation(context.getSize().width - closeButton.getSize().width, 0);
        closeButton.addMouseListener(new MouseListener() {
            @Override
            public void onHover() {

            }

            @Override
            public void onClick() {
                route(new Intent("BGameActivity", "BMainActivity"));
            }
        });

        sidebarGUI.getSideBarStoreGUI().getBuyApiaryButton().addMouseListener(new MouseListener() {
            @Override
            public void onHover() {
            }

            @Override
            public void onClick() {
                BApiary apiary = new BApiary();
                mainGUI.add(apiary.getGui());
            }
        });

        mainGUI = new Panel();
        mainGUI.setSize(Display.getWidth() - closeButton.getSize().width - Constants.SIDE_BAR_WIDTH, Display.getHeight() - 20);
        mainGUI.setLayout(new ListLayout());
        mainGUI.setLocation(Constants.SIDE_BAR_WIDTH, 0);
        mainGUI.setBgColor(Color.TRANSPARENT);

        context.add(sidebarGUI);
        context.add(closeButton);

        start(mainGUI);
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void onStart() {
        NotificationManager.push(new Notification(NotificationType.SUCCESS, "Game started !"));
    }

    @Override
    public void onEnd() {

    }
}
