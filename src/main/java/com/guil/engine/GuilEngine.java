package com.guil.engine;

import com.guil.activities.Activity;
import com.guil.annotations.GuilActivity;
import com.guil.annotations.StartingActivity;
import com.guil.annotations.WindowInfo;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.lwjgl.opengl.Display;
import org.reflections.Reflections;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Set;

@Slf4j
@Getter
public class GuilEngine {
    public static Window window = null;

    /**
     * Starting GUIL engine based game.
     *
     * @param main The main class.
     */
    @SneakyThrows
    public static void startGame(Class<?> main) {
        log.info("Trying to find natives in the root of the project.");
        File natives = new File("libs/natives/windows");
        if (!natives.exists()) {
            log.error("No such native directory in the root of the project.");
        }
        System.setProperty("org.lwjgl.librarypath", new File("libs/natives/windows").getAbsolutePath());

        log.info(String.format("Trying to start game from %s class.", main));
        Annotation[] annotations = main.getAnnotations();

        log.info("Checking for @WindowInfo annotation.");
        WindowInfo windowInfo = (WindowInfo) Arrays.stream(annotations).filter(annotation ->
                annotation instanceof WindowInfo).findFirst().orElseThrow(() ->
                new ClassNotFoundException("Cannot find main class"));

        assert windowInfo != null;

        window = new Window(windowInfo.icon(), windowInfo.title(), windowInfo.width(), windowInfo.height());

        log.info("Checking for @StartingActivity annotation.");
        StartingActivity startingActivity = (StartingActivity) Arrays.stream(annotations).filter(annotation ->
                annotation instanceof StartingActivity).findFirst().orElseThrow(() ->
                new IllegalClassFormatException("Main class should contains @StartingActivity annotation"));

        assert startingActivity != null;

        log.info(String.format("Setting starting activity to %s.", startingActivity));
        window.setStartingActivity(startingActivity.name());
        log.info("Starting GUIL engine game.");
        window.start();
    }

    /**
     * Loading all activities from all files.
     */
    public static void loadAllActivities() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        log.info("Loading all @GuilActivity annotated classes.");
        long startTime = System.nanoTime();
        Reflections ref = new Reflections("");
        Set<Class<?>> typesAnnotatedWithGuilActivity = ref.getTypesAnnotatedWith(GuilActivity.class);
        log.info(String.format("Recovered %s Activity classes.", typesAnnotatedWithGuilActivity));
        for (Class<?> cl : typesAnnotatedWithGuilActivity) {
            ((Activity) Class.forName(cl.getName()).getDeclaredConstructor().newInstance())
                    .setPersistent(cl.getAnnotation(GuilActivity.class).persistent());
        }
        long endTime = System.nanoTime();
        log.info(String.format("Loaded all Activity classes in %fs.", (endTime - startTime) / 1000000000.0F));
    }

    public static void stop() {
        log.info("Stopping GUIL engine game.");
        Display.destroy();
        System.exit(0);
    }
}
