package com.guil.engine;

import com.guil.activities.Activity;
import com.guil.managers.ActivityManager;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import static org.lwjgl.opengl.GL11.*;

/**
 * GUIL Window class.
 */
@Slf4j
@Setter
public class Window {

    public static final double FPS = 60.0;
    public static boolean tick = false;
    public static boolean render = false;

    public static Activity activity;
    public static Set<Activity> persistentActivities = new HashSet<>();
    private static int fps = 0;
    public int frames = 0;
    public int ticks = 0;
    private String title;
    private int width, height;

    public Window(String icon, String title, int width, int height) {
        this.width = width;
        this.height = height;
        this.title = title;
        try {
            log.info("Initializing OpenGL context.");
            Display.setDisplayMode(new DisplayMode(width, height));
            Display.setTitle(title);
            Display.setResizable(false);
            Display.setIcon(IconLoader.load(icon));
            log.info("Creating rendering display.");
            Display.create();
            glClearColor(0.95f, 0.95f, 0.95f, 1.0f);
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        } catch (final LWJGLException e) {
            log.error(String.format("Unable to initialize OpenGL context %s.", e.getMessage()));
            System.exit(0);
        }
        try {
            GuilEngine.loadAllActivities();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            log.error(String.format("Unable to load all @GuilActivity annotated classes %s.", e.getMessage()));
        }
    }

    /**
     * Closing window.
     */
    public static void quit() {
        GuilEngine.stop();
    }

    /**
     * @return FPS of the game.
     */
    public static int getFPS() {
        return fps;
    }

    /**
     * Setting starting activity.
     *
     * @param name The simple name of the activity.
     */
    public void setStartingActivity(String name) {
        activity = ActivityManager.find(name);
        if(activity != null) {
            activity.context.setVisible(true);
            activity.onStart();
        } else {
            log.error(String.format("Cannot find starting activity %s.", name));
            throw new NullPointerException(String.format("Cannot find starting activity %s.", name));
        }
    }

    /**
     * Starting window.
     */
    public void start() {
        long timer = System.currentTimeMillis();

        long before = System.nanoTime();
        double elapsed;
        double nanoSeconds = 1000000000.0 / FPS;

        while (!Display.isCloseRequested()) {
            Display.update();
            tick = false;
            render = false;

            long now = System.nanoTime();
            elapsed = now - before;

            if (elapsed > nanoSeconds) {
                before += nanoSeconds;
                tick = true;
                ticks++;
            } else {
                render = true;
                frames++;
            }

            if (tick) update();
            if (render) render();

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                fps = frames;
                ticks = 0;
                frames = 0;
            }
        }
    }

    /**
     * Updating the window.
     */
    public void update() {
        activity.update();
        persistentActivities.forEach(Activity::update);
    }

    /**
     * Rendering the window.
     */
    public void render() {
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, Display.getWidth(), Display.getHeight(), 0, -1, 1);
        glViewport(0, 0, width, height);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        activity.render();
    }
}
