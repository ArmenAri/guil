package com.guil.engine;

import com.guil.engine.textures.Texture;
import com.guil.enums.Color;

import static org.lwjgl.opengl.GL11.*;

public class Renderer {
    private static final Texture font = Texture.loadTexture("/assets/fonts/font.png");

    private static final String chars =
            "abcdefghijklmnopqrstuvwxyz " +
                    "0123456789:!?.,()$+-/**|***" +
                    "***************************" +
                    "****************************";

    /**
     * Draw string on the screen.
     *
     * @param str  the string to draw
     * @param x    the position on x-axis
     * @param y    the position on y-axis
     * @param size the font size of the string.
     * @see Texture#bind
     * @see Texture#unbind
     */
    public static void drawString(String str, final int x, final int y, final int size) {
        str = str.toLowerCase();
        glEnable(GL_TEXTURE_2D);
        font.bind();
        glBegin(GL_QUADS);

        for (int i = 0; i < str.length(); i++) {
            final char c = str.charAt(i);
            final int offs = i * size;
            charData(c, x + offs, y, size);
        }

        glEnd();
        font.unbind();
        glDisable(GL_TEXTURE_2D);
    }

    private static void charData(final char c, final int x, final int y, final int size) {
        final int i = chars.indexOf(c);
        final int xo = i % 27;
        final int yo = i / 27;

        glTexCoord2f((xo) / 27.0f, (yo) / 4.0f);
        glVertex2f(x, y);
        glTexCoord2f((1 + xo) / 27.0f, (yo) / 4.0f);
        glVertex2f(x + size, y);
        glTexCoord2f((1 + xo) / 27.0f, (1 + yo) / 4.0f);
        glVertex2f(x + size, y + size);
        glTexCoord2f((xo) / 27.0f, (1 + yo) / 4.0f);
        glVertex2f(x, y + size);
    }

    /**
     * Draw quad on the screen.
     *
     * @param x      the position on x-axis
     * @param y      the position on y-axis
     * @param width  the size of the quad on x-axis
     * @param height the size of the quad on y-axis
     */
    public static void drawQuad(final int x, final int y, final int width, final int height) {
        glBegin(GL_QUADS);
        glVertex2f(x, y);
        glVertex2f(x + width, y);
        glVertex2f(x + width, y + height);
        glVertex2f(x, y + height);
        glEnd();
    }

    /**
     * Draw a texture on the screen.
     *
     * @param tex    the texture to draw
     * @param x      the position on x-axis
     * @param y      the position on y-axis
     * @param width  the size of the texture on x-axis
     * @param height the size of the texture on y-axis
     * @see Texture
     */
    public static void drawTexture(final int tex, final int x, final int y, final int width, final int height) {
        final int xo = tex % 8;
        final int yo = tex / 8;
        glEnable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
        glTexCoord2f((xo) / 8.0f, (yo) / 8.0f);
        glVertex2f(x, y);
        glTexCoord2f((xo) / 8.0f, (yo) / 8.0f);
        glVertex2f(x + width, y);
        glTexCoord2f((xo) / 8.0f, (yo) / 8.0f);
        glVertex2f(x + width, y + height);
        glTexCoord2f((xo) / 7.0f, (yo) / 8.0f);
        glVertex2f(x, y + height);
        glEnd();
        glDisable(GL_TEXTURE_2D);
    }

    /**
     * Set graphic color to r, g, b, a
     *
     * @param r red component
     * @param g green component
     * @param b blue component
     * @param a alpha component
     */
    public static void color(final float r, final float g, final float b, final float a) {
        glColor4f(r, g, b, a);
    }

    /**
     * Set graphic color to color
     *
     * @param color the color to set
     * @see Color
     */
    public static void color(final Color color) {
        glColor4f(color.getR(), color.getG(), color.getB(), color.getA());
    }
}
