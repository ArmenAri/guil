package com.guil.interfaces;

/**
 * Interface for updating things.
 */
public interface Updatable {
    /**
     * Update method.
     */
    void update();
}
