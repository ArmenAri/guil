package com.guil.interfaces;

/**
 * Interface for two-state buttons.
 */
public interface SwitchMouseListener {
    /**
     * Action performed when a click is done on the two-state button.
     */
    void onCheck();
}
