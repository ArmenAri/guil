package com.guil.interfaces;

/**
 * Interface for rendering things.
 */
public interface Displayable {
    /**
     * Render method.
     */
    void render();
}
