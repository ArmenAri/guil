package com.guil.interfaces;

public interface MouseListener {

    /**
     * Action performed when a hover is done on the button.
     */
    void onHover();

    /**
     * Action performed when a click is done on the button.
     */
    void onClick();
}
