package com.guil.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Label enum.
 */
@Getter
@AllArgsConstructor
public enum LabelType {

    /**
     * Ease-of-use for BODY label.
     */
    BODY(14),

    /**
     * Ease-of-use for SUBTITLE label.
     */
    SUBTITLE(16),

    /**
     * Ease-of-use for TITLE label.
     */
    TITLE(24);

    /**
     * Font size of the label.
     */
    private final int fontSize;
}
