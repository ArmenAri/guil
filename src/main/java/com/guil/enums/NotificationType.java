package com.guil.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum NotificationType {
    SUCCESS(Color.GREEN), ERROR(Color.RED), INFO(Color.BLUE);
    private Color color;
}
