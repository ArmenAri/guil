package com.guil.enums;

/**
 * Alignment enum.
 */
public enum Alignment {
    /**
     * Ease-of-use for CENTER alignment.
     */
    CENTER,
    /**
     * Ease-of-use for LEFT alignment.
     */
    LEFT,
    /**
     * Ease-of-use for RIGHT alignment.
     */
    RIGHT
}
