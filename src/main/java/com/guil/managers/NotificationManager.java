package com.guil.managers;

import com.guil.components.Notification;

import java.util.ArrayDeque;

/**
 * Class that manages <code>Notification</code>.
 * The <code>NotificationManager</code> uses <code>ArrayDeque</code> to
 * manage <code>Notification</code> instances. The <code>FIFO</code> system is used.
 */
public class NotificationManager {
    private static final ArrayDeque<Notification> root = new ArrayDeque<>();

    public static void render() {
        Notification notification = root.peekFirst();
        if (notification != null) {
            notification.show();
        }
    }

    public static void update() {
        Notification notification = root.peekFirst();
        if (notification != null) {
            notification.update();
            if (notification.isFinished())
                pop();
        }
    }

    /**
     * Pushing new <code>Notification</code> into <code>NotificationManger</code>.
     *
     * @param notification The notification to add to the deque.
     */
    public static void push(Notification notification) {
        root.addLast(notification);
    }

    private static void pop() {
        root.pollFirst();
    }
}
