package com.guil.managers;

import com.guil.activities.Activity;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to manage multiple activities.
 */
public class ActivityManager {
    /**
     * List of all activities.
     */
    private static final List<Activity> activities = new ArrayList<>();

    /**
     * Adding activity into manager.
     *
     * @param a the activity to add.
     */
    public static void add(Activity a) {
        activities.add(a);
    }

    /**
     * Method that ables to find an Activity from its name.
     *
     * @param name The simple name of the Activity.
     * @return The activity.
     */
    public static Activity find(String name) {
        return activities.stream().filter(a -> a.getName().equals(name)).findFirst().orElse(null);
    }
}
