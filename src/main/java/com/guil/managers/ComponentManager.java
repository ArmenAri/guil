package com.guil.managers;

import com.guil.components.Component;
import com.guil.components.containers.Container;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ComponentManager {

    private static final List<Component> root = new ArrayList<>();

    public static void render() {
        for(int i = 0; i < root.size(); ++i) {
            Component component = root.get(i);
            if (component instanceof Container) {
                Container container = (Container) component;
                if (container.insideTheViewport())
                    Arrays.stream(container.getComponents()).forEach(Component::show);
            }
        }
    }

    public static void update() {
        for(int i = 0; i < root.size(); ++i) {
            Component component = root.get(i);
            if (component instanceof Container) {
                Container container = (Container) component;
                Arrays.stream(container.getComponents()).forEach(Component::update);
            }
        }
    }

    public static void add(Container container) {
        root.add(container);
    }
}
