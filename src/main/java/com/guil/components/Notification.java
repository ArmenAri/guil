package com.guil.components;

import com.guil.activities.Activity;
import com.guil.engine.Renderer;
import com.guil.engine.Window;
import com.guil.enums.Color;
import com.guil.enums.NotificationType;
import lombok.Getter;
import lombok.Setter;
import org.lwjgl.opengl.Display;

@Setter
@Getter
public class Notification extends Component {

    private final String message;
    private final Color color;
    private final Activity context;
    private int step = 1;
    private float life = 0;
    private boolean finished;

    public Notification(NotificationType notificationType, String message) {
        this(notificationType.getColor(), message, null);
    }

    public Notification(NotificationType notificationType, String message, Activity context) {
        this(notificationType.getColor(), message, context);
    }

    private Notification(Color color, String message, Activity context) {
        this.color = color;
        this.message = message;
        this.context = context;
        this.setSize(Display.getWidth(), 32);
        this.setLocation(0, -this.height);
    }

    /**
     * Render the component.
     */
    @Override
    public void show() {
        Renderer.color(color.getR(), color.getG(), color.getB(), 0.8f);
        Renderer.drawQuad(this.x, this.y, this.width, this.height);
        Renderer.color(1, 1, 1, 1);
        Renderer.drawString(message, (Display.getWidth() >> 1) - (24 * message.length() >> 1), 7 + y, 24);
    }

    /**
     * Update the component.
     */
    @Override
    public void update() {
        if ((this.context != null) && (Window.activity != context)) this.finished = true;
        if (this.life > 250) this.finished = true;
        if (this.life > 100) this.step = -1;
        if ((this.y >= 0) && (this.life <= 200)) this.step = 0;

        this.y += this.step;
        this.life++;
    }
}
