package com.guil.components.layouts;

import com.guil.components.Component;
import com.guil.components.containers.Container;
import com.guil.components.layouts.interfaces.LayoutManager;

public class ListLayout implements LayoutManager {

    @Override
    public void bind(final Component c, final Container parent, final Component last) {
        if (!c.isOutSideTheParentV(last)) c.setLocation(last.getLocation().x,
                last.getLocation().y + last.getSize().height + parent.getTopAndBottomPadding());

        else c.setLocation(last.getLocation().x + last.getSize().width + parent.getLeftAndRightPadding(),
                parent.getLocation().y + parent.getTopAndBottomPadding());
    }
}