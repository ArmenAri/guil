package com.guil.components.layouts.interfaces;

import com.guil.components.Component;
import com.guil.components.containers.Container;

public interface LayoutManager {
    void bind(final Component c, final Container parent, final Component last);
}
