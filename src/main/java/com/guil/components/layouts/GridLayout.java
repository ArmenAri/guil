package com.guil.components.layouts;

import com.guil.components.Component;
import com.guil.components.containers.Container;
import com.guil.components.layouts.interfaces.LayoutManager;

public class GridLayout implements LayoutManager {

    @Override
    public void bind(final Component c, final Container parent, final Component last) {
        if (!c.isOutSideTheParentH(last)) c.setLocation(last.getLocation().x +
                last.getSize().width + parent.getLeftAndRightPadding(), last.getLocation().y);

        else c.setLocation(parent.getLocation().x + parent.getLeftAndRightPadding(),
                last.getLocation().y + last.getSize().height + parent.getTopAndBottomPadding());
    }
}