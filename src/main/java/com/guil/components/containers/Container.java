package com.guil.components.containers;

import com.guil.components.Component;
import com.guil.components.layouts.interfaces.LayoutManager;
import com.guil.enums.Color;
import com.guil.managers.ComponentManager;
import org.lwjgl.opengl.Display;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Container extends Component {

    private static final Component[] EMPTY_ARRAY = new Component[0];
    /**
     * The components in this container.
     *
     * @see #add
     * @see #getComponents
     */
    private final List<Component> components = new ArrayList<>();
    /**
     * Top and bottom padding.
     */
    int TOP_AND_BOTTOM_PADDING = 10;
    /**
     * Left and right padding.
     */
    int LEFT_AND_RIGHT_PADDING = 10;
    /**
     * The background color of this container.
     */
    Color bgColor;
    private LayoutManager layout;

    /**
     * Constructs a new Container.
     */
    public Container() {
        ComponentManager.add(this);
    }

    /**
     * Gets the number of components in this panel.
     *
     * @return the number of components in this panel.
     * @see #getComponent
     */
    public int getComponentCount() {
        return countComponents();
    }

    public int countComponents() {
        return components.size();
    }

    /**
     * Set visible variable of this container to visible.
     *
     * @param visible
     */
    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
        for (final Component c : components) {
            c.setVisible(visible);
        }
    }

    /**
     * Gets the last component in this container.
     *
     * @return the last component in this container.
     */
    public Component getLastComponent() {
        return getComponent(countComponents() - 1);
    }

    /**
     * Gets the nth component in this container.
     *
     * @param n the index of the component to get.
     * @return the n<sup>th</sup> component in this container.
     * @throws ArrayIndexOutOfBoundsException if the n<sup>th</sup> value does
     *                                        not exist.
     */
    public Component getComponent(final int n) {
        try {
            return components.get(n);
        } catch (final IndexOutOfBoundsException z) {
            throw new ArrayIndexOutOfBoundsException("No such child: " + n);
        }
    }

    /**
     * Gets all the components in this container.
     * <p>
     * Note: This method should be called under AWT tree lock.
     *
     * @return an array of all the components in this container.
     */
    public Component[] getComponents() {
        return getComponents_NoClientCode();
    }

    final Component[] getComponents_NoClientCode() {
        return components.toArray(EMPTY_ARRAY);
    }

    /**
     * Appends the specified component to the end of this container.
     * <p>
     * This method changes layout-related information, and therefore, invalidates
     * the component hierarchy.
     *
     * @param comps the list of component to be added
     */
    public void add(final Component... comps) {
        Arrays.stream(comps).forEachOrdered(c -> {
            c.setParent(this);
            components.add(c);
        });
    }

    /**
     * @return the layout of the container.
     */
    public LayoutManager getLayout() {
        return layout;
    }

    /**
     * Sets the layout of the container.
     */
    public void setLayout(final LayoutManager layout) {
        this.layout = layout;
    }

    /**
     * Sets the background color of the container.
     */
    public void setBgColor(final Color bgColor) {
        this.bgColor = bgColor;
    }

    /**
     * Sets the container padding to topAndBottom, leftAndRight.
     *
     * @param topAndBottom
     * @param leftAndRight
     * @see #getTopAndBottomPadding
     * @see #getLeftAndRightPadding
     */
    public void setPadding(final int topAndBottom, final int leftAndRight) {
        TOP_AND_BOTTOM_PADDING = topAndBottom;
        LEFT_AND_RIGHT_PADDING = leftAndRight;
    }

    /**
     * @return the top and bottom padding of the container. That defines the way of
     * children component layouts.
     */
    public int getTopAndBottomPadding() {
        return TOP_AND_BOTTOM_PADDING;
    }

    /**
     * @return the left and right padding of the container. That defines the way of
     * children component layouts.
     */
    public int getLeftAndRightPadding() {
        return LEFT_AND_RIGHT_PADDING;
    }

    /**
     * Bind the component to its parent component. This method changes the positions
     * of the component to match with parent ones.
     */
    public void bind(final Component c) {
        if (getComponentCount() > 0 && layout != null) {
            if (c instanceof Container && ((Container) c).getComponentCount() > 0) {
                Container container = ((Container) c);
                layout.bind(container, this, getLastComponent());
                Arrays.stream(container.getComponents()).forEach(container::bind);
            } else layout.bind(c, this, getLastComponent());
        } else {
            if (c instanceof Container && ((Container) c).getComponentCount() > 0) {
                Container container = ((Container) c);
                container.setLocation(x + LEFT_AND_RIGHT_PADDING + container.getLocation().x, y + TOP_AND_BOTTOM_PADDING + container.getLocation().y);
                Component[] containerComponents = container.getComponents();
                for (Component child : containerComponents) {
                    child.setLocation(x + LEFT_AND_RIGHT_PADDING + child.getLocation().x, y + TOP_AND_BOTTOM_PADDING + child.getLocation().y);
                }
            } else
                c.setLocation(x + LEFT_AND_RIGHT_PADDING + c.getLocation().x, y + TOP_AND_BOTTOM_PADDING + c.getLocation().y);
        }
    }

    public boolean insideTheViewport() {
        Rectangle screen = new Rectangle(-1, -1, Display.getWidth() + 1, Display.getHeight() + 1);
        return screen.contains(this.getBounds());
    }
}
