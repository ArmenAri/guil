package com.guil.components.containers;

import com.guil.engine.Renderer;

public class Panel extends Container {

    /**
     * Constructs the panel.
     */
    public Panel() {
        super();
    }

    @Override
    public void show() {
        if (isVisible() && insideTheViewport() && getParent() != null && getParent().insideTheViewport()) {
            Renderer.color(bgColor);
            Renderer.drawQuad(x, y, width, height);
        }
    }

    @Override
    public void update() {

    }
}
