package com.guil.components;

import com.guil.engine.Renderer;
import com.guil.enums.Color;
import com.guil.enums.LabelType;

import java.util.Objects;

public class Label extends Component {
    private static final String base = "label";
    private static int nameCounter = 0;
    /**
     * The text of this label.
     *
     * @serial
     * @see #getText()
     * @see #setText(String)
     */
    String text;
    /**
     * The color of this label.
     *
     * @serial
     * @see #getColor()
     * @see #setColor(Color)
     */
    Color color = Color.WHITE;
    /**
     * The font size of this label.
     *
     * @serial
     * @see #getFontSize()
     * @see #setFontSize(int)
     */
    int fontSize = 24;
    LabelType labelType = LabelType.TITLE;

    /**
     * Constructs an empty label. The text of the label is the text string
     * <code>"label0"</code>, <code>"label1"</code>, etc.
     */
    public Label() {
        this(constructComponentName());
    }

    public Label(final String text) {
        super();
        this.text = text;
        width = fontSize * text.length();
        height = fontSize;
    }

    /**
     * Constructs a new label that presents the specified string of text with the
     * specified alignment. Possible values for <code>alignment</code> are
     * <code>Label.LEFT</code>, <code>Label.RIGHT</code>, and
     * <code>Label.CENTER</code>.
     *
     * @param text the string that the label presents. A <code>null</code> value
     *             will be accepted without causing a NullPointerException to be
     *             thrown.
     */
    public Label(final String text, LabelType labelType) {
        this.text = text;
        this.labelType = labelType;
        fontSize = this.labelType.getFontSize();
        width = fontSize * text.length();
        height = fontSize;
    }

    /**
     * Construct a name for this component when the name is <code>null</code>.
     */
    static String constructComponentName() {
        synchronized (Label.class) {
            return base + nameCounter++;
        }
    }

    /**
     * Gets the text of this label.
     *
     * @return the text of this label, or <code>null</code> if the text has been set
     * to <code>null</code>.
     * @see #setText
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the text for this label to the specified text.
     *
     * @param text the text that this label displays. If <code>text</code> is
     *             <code>null</code>, it is treated for display purposes like an
     *             empty string <code>""</code>.
     * @see #getText
     */
    public void setText(final String text) {
        synchronized (this) {
            if (!Objects.equals(this.text, text)) {
                this.text = text;
            }
        }
    }

    /**
     * Gets the color of this label.
     *
     * @return the color of this label, or <code>null</code> if the color has been
     * set to <code>null</code>.
     * @see #setColor
     */
    public Color getColor() {
        return color;
    }

    /**
     * Sets the color for this label to the specified color.
     *
     * @param color the color that this label colored.
     * @see #getColor
     */
    public void setColor(final Color color) {
        synchronized (this) {
            if (!Objects.equals(this.color, color)) this.color = color;
        }
    }

    /**
     * Gets the fontSize of this label.
     *
     * @return the fontSize of this label.
     * @see #setFontSize
     */
    public int getFontSize() {
        return fontSize;
    }

    /**
     * Sets the fontSize for this label to the specified fontSize.
     *
     * @param fontSize the fontSize that this label sized.
     * @see #getFontSize
     */
    public void setFontSize(final int fontSize) {
        synchronized (this) {
            this.fontSize = fontSize;
        }
    }

    public LabelType getLabelType() {
        return labelType;
    }

    @Override
    public void show() {
        if (isVisible()) {
            Renderer.color(color);
            if (labelType.name().equals("TITLE"))
                Renderer.drawQuad(x, y + fontSize, text.length() * fontSize, 2);
            Renderer.drawString(text, x, y, fontSize);
        }
    }

    @Override
    public void update() {

    }
}
