package com.guil.components;

import com.guil.components.containers.Container;

import java.awt.*;
import java.util.Objects;

public abstract class Component {

    public static final int MANUAL_BOUNDING = 0;
    public static final int AUTOMATIC_BOUNDING = 1;
    /**
     * Ease-of-use constant for <code>getAlignmentY</code>. Specifies an alignment
     * to the bottom of the component.
     *
     * @see #getAlignmentY
     */
    public static final float BOTTOM_ALIGNMENT = 1.0f;
    /**
     * Ease-of-use constant for <code>getAlignmentY</code> and
     * <code>getAlignmentX</code>. Specifies an alignment to the center of the
     * component.
     *
     * @see #getAlignmentX
     * @see #getAlignmentY
     */
    public static final float CENTER_ALIGNMENT = 0.5f;
    /**
     * Ease-of-use constant for <code>getAlignmentX</code>. Specifies an alignment
     * to the left of the component.
     *
     * @see #getAlignmentX
     */
    public static final float LEFT_ALIGNMENT = 0.0f;
    /**
     * Ease-of-use constant for <code>getAlignmentX</code>. Specifies an alignment
     * to the right of the component.
     *
     * @see #getAlignmentX
     */
    public static final float RIGHT_ALIGNMENT = 1.0f;
    /**
     * Ease-of-use constant for <code>getAlignmentY</code>. Specifies an alignment
     * to the top of the component.
     *
     * @see #getAlignmentY
     */
    public static final float TOP_ALIGNMENT = 0.0f;
    /**
     * The parent of the object. It may be <code>null</code> for the top-level
     * components.
     *
     * @see #getParent
     * @see #setParent
     */
    protected transient Container parent;
    /**
     * The x position of the component in the parent's coordinate system.
     *
     * @serial
     * @see #getLocation
     */
    protected int x;
    /**
     * The y position of the component in the parent's coordinate system.
     *
     * @serial
     * @see #getLocation
     */
    protected int y;
    /**
     * The width of the component.
     *
     * @serial
     * @see #getSize
     */
    protected int width;
    /**
     * The height of the component.
     *
     * @serial
     * @see #getSize
     */
    protected int height;
    /**
     * Variable that indicate if the component is visible or no.
     */
    protected boolean visible = false;
    /**
     * Variable that indicate if the component is bounded or no.
     */
    protected boolean isBounded = false;

    /**
     * Constructs a new component.
     */
    protected Component() {
    }

    /**
     * @return a <code>Rectangle</code> object that indicates the bounds of this
     * component.
     * @see #setBounds
     */
    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }

    /**
     * Moves and resizes this component.
     *
     * @param rect
     */
    public void setBounds(final Rectangle rect) {
        setBounds(rect.x, rect.y, rect.width, rect.height, AUTOMATIC_BOUNDING);
    }

    /**
     * Moves and resizes this component.
     *
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void setBounds(final int x, final int y, final int width, final int height) {
        setBounds(x, y, width, height, MANUAL_BOUNDING);
    }

    /**
     * Moves and resizes this component.
     *
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void setBounds(final int x, final int y, final int width, final int height, final int TYPE) {
        final boolean resized = (this.width != width) || (this.height != height);
        final boolean moved = (this.x != x) || (this.y != y);
        if (!resized && !moved) {
            return;
        }
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        switch (TYPE) {
            case MANUAL_BOUNDING -> isBounded = true;
            default -> {
            }
        }
    }

    private Point location() {
        return new Point(x, y);
    }

    /**
     * @return a <code>Point</code> object that indicates the location of this
     * component.
     * @see #setLocation
     */
    public Point getLocation() {
        return location();
    }

    /**
     * Moves this component to a new location.
     *
     * @param x
     * @param y
     */
    public void setLocation(final int x, final int y) {
        setBounds(x, y, width, height, AUTOMATIC_BOUNDING);
    }

    private Dimension size() {
        return new Dimension(width, height);
    }

    /**
     * @return a <code>Dimension</code> object that indicates the size of this
     * component.
     * @see #setSize
     */
    public Dimension getSize() {
        return size();
    }

    /**
     * Resizes this component so that it has width width and height height.
     *
     * @param width
     * @param height
     */
    public void setSize(final int width, final int height) {
        setBounds(x, y, width, height, AUTOMATIC_BOUNDING);
    }

    /**
     * Gets the parent of this component.
     *
     * @return the parent container of this component
     */
    public Container getParent() {
        return parent;
    }

    /**
     * Sets the parent of this component.
     *
     * @param parent
     */
    public void setParent(final Container parent) {
        this.parent = Objects.requireNonNull(parent, "Parent container cannot be null !");
        if (!isBounded)
            parent.bind(this);
    }

    /**
     * @return The alignment along x axis. This specifies how the component would
     * like to be aligned relative to other components. The value should be
     * a number between 0 and 1 where 0 represents alignment along the
     * origin, 1 is aligned the furthest away from the origin, 0.5 is
     * centered, etc.
     */
    public float getAlignmentX() {
        return CENTER_ALIGNMENT;
    }

    /**
     * @return The alignment along y axis. This specifies how the component would
     * like to be aligned relative to other components. The value should be
     * a number between 0 and 1 where 0 represents alignment along the
     * origin, 1 is aligned the furthest away from the origin, 0.5 is
     * centered, etc.
     */
    public float getAlignmentY() {
        return CENTER_ALIGNMENT;
    }

    /**
     * Checks if the next component will be out of the parent container.
     *
     * @param last
     */
    public boolean isOutSideTheParentV(final Component last) {
        return ((last.y + last.height + height + parent.getTopAndBottomPadding()) > (parent.y + parent.height));
    }

    /**
     * Checks if the next component will be out of the parent container.
     *
     * @param last
     */
    public boolean isOutSideTheParentH(final Component last) {
        return ((last.x + last.width + width + parent.getLeftAndRightPadding()) > (parent.x + parent.width));
    }

    /**
     * Set the width of the component to the width of the parent component.
     */
    public void occupyMaximumWidth() {
        if (parent == null)
            throw new NullPointerException("The component must have a parent before setting its width to maximum.");
        setBounds(x, y, parent.width - (2 * parent.getLeftAndRightPadding()), height, 1);
    }

    /**
     * @return visible
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Shows or hides this component depending on the value of parameter visible.
     *
     * @param visible
     */
    public void setVisible(final boolean visible) {
        this.visible = visible;
    }

    /**
     * Render the component.
     */
    public abstract void show();

    /**
     * Update the component.
     */
    public abstract void update();
}
