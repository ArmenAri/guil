package com.guil.components.buttons;

import com.guil.engine.Renderer;
import com.guil.enums.Color;
import com.guil.interfaces.MouseListener;
import com.guil.interfaces.SwitchMouseListener;

public class Switch extends Button {

    private static final int border = 3;
    private boolean isChecked = false;

    public Switch(SwitchMouseListener onCheckMouseListener, SwitchMouseListener onUncheckMouseListener) {
        setSize(75, 25);
        MouseListener mouseListener = new MouseListener() {
            @Override
            public void onHover() {
            }

            @Override
            public void onClick() {
                isChecked = !isChecked;
                if (isChecked)
                    onCheckMouseListener.onCheck();
                else
                    onUncheckMouseListener.onCheck();
            }
        };
        addMouseListener(mouseListener);
    }

    @Override
    public void showButton(Color color) {
        Renderer.color(Color.DARK_GRAY);
        Renderer.drawQuad(x, y, width + (2 * border), height + (2 * border));
        Renderer.color(color);
        Renderer.drawQuad(x + border, y + border, width >> 1, height);
        Renderer.color(Color.LIGHT_GRAY);
        Renderer.drawQuad(!isChecked ? (x + border) : (x + (width >> 1) + border), y + border, width >> 1, height);
    }

    @Override
    public void show() {
        if (isVisible()) {
            showButton(Color.GREEN);
            onClick();
        }
    }
}
