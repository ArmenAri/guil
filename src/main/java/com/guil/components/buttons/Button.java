package com.guil.components.buttons;

import com.guil.components.Component;
import com.guil.engine.Renderer;
import com.guil.enums.Alignment;
import com.guil.enums.Color;
import com.guil.interfaces.MouseListener;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import java.awt.*;
import java.awt.event.ActionListener;

public class Button extends Component {

    /**
     * The base label of the button.
     *
     * @see #getText
     */
    private static final String base = "button";
    /**
     * The integer which will count the number of buttons and place itself at the
     * end of the label of the button.
     *
     * @see #getText
     */
    private static int nameCounter = 0;
    /**
     * The label of the button. It may be <code>null</code> but it's name will be
     * replaced by <code>button0</code> for the first button <code>button1</code>
     * for the second etc.
     *
     * @see #getText
     */
    String text;
    /**
     * The {@link ActionListener} of the button. It may be <code>null</code>.
     *
     * @see #addMouseListener(MouseListener)
     */
    transient MouseListener mouseListener;
    /**
     * The base size of font of the button.
     */
    int fontSize = 24;

    Alignment alignment = Alignment.LEFT;

    /**
     * Constructs a new button.
     */
    public Button() throws HeadlessException {
        this(constructComponentName());
    }

    /**
     * Constructs a new button with specified label.
     *
     * @param label
     */

    public Button(final String label) throws HeadlessException {
        super();
        text = label;
        height = 32;
        width = this.text.length() * fontSize + 20;
    }

    /**
     * Construct a name for this component when the name is <code>null</code>.
     */
    static String constructComponentName() {
        synchronized (Button.class) {
            return base + nameCounter++;
        }
    }

    /**
     * @return The label of the button.
     */
    public String getText() {
        return text;
    }

    /**
     * Set the label of the button to label
     *
     * @param text the label
     */
    public void setText(final String text) {
        if (!text.equals(this.text) && (this.text == null) || !this.text.equals(text)) this.text = text;
    }

    /**
     * Set the fontSize of the button to fontSize
     *
     * @param fontSize the fontSize
     */
    public void setFontSize(final int fontSize) {
        synchronized (this) {
            this.fontSize = fontSize;
        }
    }

    /**
     * Add an {@link MouseListener} to the button.
     *
     * @param m the {@link MouseListener}
     */
    public synchronized void addMouseListener(final MouseListener m) {
        if (m == null) return;
        mouseListener = m;
    }

    /**
     * @return True if the button is hovered else false
     */
    private boolean onHover() {
        if (getBounds().contains(Mouse.getX(), Display.getHeight() - Mouse.getY())) {
            showButton(Color.LIGHT_GRAY);
            if (mouseListener != null)
                mouseListener.onHover();
            return true;
        }
        return false;
    }

    /**
     */
    protected void onClick() {
        if (getBounds().contains(Mouse.getX(), Display.getHeight() - Mouse.getY())) {
            while (Mouse.next()) {
                if (Mouse.isButtonDown(0)) {
                    showButton(Color.MIDDLE_GRAY);
                    if (mouseListener != null)
                        mouseListener.onClick();
                    return;
                }
            }
        }
    }

    /**
     * @return The recalculated length of the button's label to match with button's
     * width
     */
    private int getRecalculatedLabelLength() {
        return Math.min(text.length(), ((width - 10) / fontSize));
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    /**
     * Render the button.
     *
     * @param color color of the button.
     */
    public void showButton(final Color color) {
        Renderer.color(color);
        Renderer.drawQuad(x, y, width, height);
        Renderer.color(Color.WHITE);

        int alignedX = 0;

        switch (alignment) {
            case CENTER -> alignedX = x + width / 2 - text.length() * fontSize / 2;
            case LEFT -> alignedX = x + 11;
            case RIGHT -> alignedX = x + width - text.length() * fontSize;
        }

        Renderer.drawString(text.substring(0, getRecalculatedLabelLength()), alignedX, y + height / 2 - fontSize / 2, fontSize);
    }

    @Override
    public void show() {
        if (isVisible()) {
            showButton(Color.GRAY);
            if (onHover()) {
                onClick();
            }
        }
    }

    @Override
    public void update() {

    }
}