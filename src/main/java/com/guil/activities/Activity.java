package com.guil.activities;

import com.guil.components.Component;
import com.guil.components.containers.Panel;
import com.guil.enums.Color;
import com.guil.interfaces.Displayable;
import com.guil.interfaces.Updatable;
import com.guil.managers.ActivityManager;
import com.guil.managers.ComponentManager;
import com.guil.managers.NotificationManager;
import lombok.Getter;
import lombok.Setter;
import org.lwjgl.opengl.Display;

/**
 * Class for activities.
 */
@Setter
@Getter
public abstract class Activity implements Displayable, Updatable {

    /**
     * Name of the Activity.
     */
    private final String name;
    /**
     * Main context of the Activity
     *
     * @see Panel
     */
    public Panel context;
    /**
     * Name of the Activity.
     */
    private boolean persistent;

    /**
     * Activity class that ables to components to render and update.
     */
    public Activity() {
        name = this.getClass().getSimpleName();
        context = new Panel();
        context.setLocation(0, 0);
        context.setPadding(0, 0);
        context.setSize(Display.getWidth(), Display.getHeight());
        context.setBgColor(Color.WHITE);
        context.setLayout(null);
        ActivityManager.add(this);
    }

    @Override
    public void render() {
        ComponentManager.render();
        NotificationManager.render();
    }

    @Override
    public void update() {
        ComponentManager.update();
        NotificationManager.update();
    }

    /**
     * Create route from one activity to another.
     *
     * @param intent The intent.
     */
    protected void route(Intent intent) {
        intent.processIntent();
    }

    /**
     * Start activity with `main` component.
     *
     * @param main The main component.
     */
    public void start(Component main) {
        context.add(main);
    }

    /**
     * @return name of the activity.
     */
    public String getName() {
        return name;
    }

    /**
     * Called when activity starts.
     */
    public abstract void onStart();

    /**
     * Called when activity ends.
     */
    public abstract void onEnd();
}
