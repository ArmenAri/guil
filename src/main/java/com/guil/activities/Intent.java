package com.guil.activities;

import com.guil.engine.Window;
import com.guil.managers.ActivityManager;

/**
 * Intent class.
 */
public class Intent {
    /**
     * Source activity.
     */
    private final Activity source;
    /**
     * Destination activity.
     */
    private final Activity destination;

    /**
     * Construct an intent from `source` and `destination` activities.
     *
     * @param source      the source activity.
     * @param destination the destination activity.
     */
    public Intent(String source, String destination) {
        this.source = ActivityManager.find(source);
        this.destination = ActivityManager.find(destination);
    }

    /**
     * Processing intent.
     */
    public void processIntent() {
        source.context.setVisible(false);
        destination.context.setVisible(true);
        Window.activity = destination;
        if (source.isPersistent())
            Window.persistentActivities.add(source);
        source.onEnd();
        destination.onStart();
    }
}
