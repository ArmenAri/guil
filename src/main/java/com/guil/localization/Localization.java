package com.guil.localization;

import java.util.Locale;
import java.util.ResourceBundle;

public class Localization {
    private static final String language = "en";
    private static final String country = "EN";

    private static final Locale locale = new Locale(language, country);

    public static ResourceBundle translation = ResourceBundle.getBundle("locale", locale);
}
