package com.guil.annotations;

import java.lang.annotation.*;

/**
 * Annotation that indicate to the library
 * information about the game <code>Window</code>
 * and instantiate it with given parameters.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface WindowInfo {
    /**
     * Window icon resource file path.
     *
     * @return Icon resource file path.
     */
    String icon();

    /**
     * Window title.
     *
     * @return Window title.
     */
    String title();

    /**
     * Window width.
     *
     * @return Window width.
     */
    int width() default 1280;

    /**
     * Window height.
     *
     * @return Window height.
     */
    int height() default 720;
}
