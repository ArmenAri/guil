package com.guil.annotations;

import java.lang.annotation.*;

/**
 * Annotation that indicates that the class
 * that follows this annotation is an <code>Activity</code> or not.
 * This is helping the engine to optimize <code>Component</code> instantiation.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface GuilActivity {
    /**
     * Indicates if the <code>Activity</code> should
     * update in background.
     * <p>
     * Be careful, this attribute highly impacts the performances of the game.
     *
     * @return Persistence of the <code>Activity</code>
     */
    boolean persistent() default false;
}
