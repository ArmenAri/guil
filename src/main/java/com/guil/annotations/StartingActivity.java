package com.guil.annotations;

import java.lang.annotation.*;

/**
 * Annotation that indicate to the library
 * the starting <code>Activity</code> of the game.
 * This annotation is mandatory when <code>WindowInfo</code>
 * annotation is present.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface StartingActivity {
    String name();
}
